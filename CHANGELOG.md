# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.0] - 2022-01-26
### Added
- Tooltip for table item titles.

## [1.1.0] - 2022-01-22
### Added
- Translation support.

## [1.0.1] - 2022-01-20
### Added
- MIT license.
- Highlight the highest die in a roll.

### Fixed
- Fix text overflow for table item titles.

## [1.0.0] - 2022-01-19
### Changed
- Full rewrite for 1.0.0.

## [0.2.0] - 2021-06-23
### Added
- Difficulty penalty for dice rolls.

## [0.1.0] - 2021-06-19
### Added
- Initial version.
